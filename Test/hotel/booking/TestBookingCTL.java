package hotel.booking;

import hotel.credit.CreditAuthorizer;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.helpers.CreditCardHelper;
import hotel.entities.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import org.junit.jupiter.api.function.Executable;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class TestBookingCTL {


    @Mock CreditAuthorizer authorizer;
    @Mock CreditCardHelper creditCardHelper;
    @Mock Guest guest;
    @Mock Room room;
    @Mock CreditCard creditCard;
    @Mock BookingUI bookingUI;
    @Mock Hotel hotel;

    BookingCTL control;
    RoomType selectedRoomType;
    int phoneNumber;
    int occupantNumber;
    Date arrivalDate;
    int stayLength;
    int cardNum;
    CreditCardType cardType;
    long confNum;
    private double cost;
    static SimpleDateFormat format;
    int ccv;
    String roomDescription;


    ArgumentCaptor<BookingUI.State> uiStateCaptor = ArgumentCaptor.forClass(BookingUI.State.class);

    @BeforeAll
    static void setUpBeforeClass() {
        format = new SimpleDateFormat("dd-MM-yyyy");
    }


    @AfterAll
    static void tearDownAfterClass() {
    }


    @BeforeEach
    void setUp() throws ParseException {
        MockitoAnnotations.initMocks(this);

        control = new BookingCTL(hotel);
        control.bookingUI = bookingUI;
        control.authorizer = authorizer;
        control.creditCardHelper = creditCardHelper;

        arrivalDate = format.parse("11-11-2011");
        stayLength = 1;
        occupantNumber = 1;
        cardNum = 1;
        cardType = CreditCardType.VISA;
        cost = 111.11;
        confNum = 11112011101L;
        ccv = 1;
        selectedRoomType = RoomType.SINGLE;
        roomDescription = selectedRoomType.getDescription();
    }


    @AfterEach
    void tearDown() {
    }


    @Test
    void testBookingCTLPhoneNumberEntered_Exception_WrongState(){
        //arrange
        control.state = BookingCTL.State.COMPLETED;
        assertNotSame(control.state, BookingCTL.State.PHONE);

        //act
        Executable e = () -> control.phoneNumberEntered(phoneNumber);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertEquals("BookingCTL: phoneNumberEntered : bad state : " + control.state, t.getMessage());
    }


    @Test
    void testPhoneNumberEtered_Correct_NotRegistered(){
        // arrange
        control.state = BookingCTL.State.PHONE;

        //act
        control.phoneNumberEntered(phoneNumber);
        verify(bookingUI).setState(uiStateCaptor.capture());

        //assert
        assertSame(control.state, BookingCTL.State.REGISTER);
        assertSame(uiStateCaptor.getValue(), BookingUI.State.REGISTER);
    }


    @Test
    void testPhoneNumberEntered_Correct_GuestExists(){
        //arrange
        assertSame(control.state, BookingCTL.State.PHONE);
        when(hotel.isRegistered(anyInt())).thenReturn(true);
        when(hotel.findGuestByPhoneNumber(anyInt())).thenReturn(guest);
        when(guest.getName()).thenReturn("Name");
        when(guest.getAddress()).thenReturn("Address");
        when(guest.getPhoneNumber()).thenReturn(1);

        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> addressCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> phoneCaptor = ArgumentCaptor.forClass(Integer.class);

        //act
        control.phoneNumberEntered(1);

        //assert
        verify(hotel).isRegistered(anyInt());
        verify(hotel).findGuestByPhoneNumber(anyInt());
        verify(bookingUI).displayGuestDetails(nameCaptor.capture(), addressCaptor.capture(), phoneCaptor.capture());
        verify(bookingUI).setState(uiStateCaptor.capture());

        assertEquals("Name", nameCaptor.getValue());
        assertEquals("Address", addressCaptor.getValue());
        assertEquals(1, (int) phoneCaptor.getValue());
        assertSame(BookingUI.State.ROOM, uiStateCaptor.getValue());
        assertSame(BookingCTL.State.ROOM, control.state);
    }


    @Test
    void testCreditDetailsEntered_Approved(){
        //arrange
        control.state = BookingCTL.State.CREDIT;
        control.room = room;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;
        control.guest = guest;
        control.occupantNumber = occupantNumber;
        control.cost = cost;


        ArgumentCaptor<String> descCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> numCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Date> dateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<String> nameCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> vendorCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> cardNumCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);
        ArgumentCaptor<Long> confNumCaptor = ArgumentCaptor.forClass(Long.class);
        ArgumentCaptor<Integer> stayCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<BookingUI.State> uiStateCaptor = ArgumentCaptor.forClass(BookingUI.State.class);


        when(creditCardHelper.makeCreditCard(any(), anyInt(), anyInt())).thenReturn(creditCard);
        when(authorizer.authorize(any(), anyDouble())).thenReturn(true);
        when(hotel.book(room, guest, arrivalDate, stayLength, occupantNumber, creditCard))
                .thenReturn(confNum);
        when(room.getDescription()).thenReturn("Description");
        when(room.getId()).thenReturn(101);
        when(guest.getName()).thenReturn("A_Name");
        when(creditCard.getVendor()).thenReturn(cardType.getVendor());
        when(creditCard.getNumber()).thenReturn(cardNum);

        assertSame(control.state, BookingCTL.State.CREDIT);

        //act
        control.creditDetailsEntered(CreditCardType.VISA, 1, 1 );

        // assert
        verify(creditCardHelper).makeCreditCard(any(), anyInt(), anyInt());
        verify(authorizer).authorize(creditCard, cost);
        verify(hotel).book(room, guest, arrivalDate, stayLength, occupantNumber, creditCard);

        verify(bookingUI).displayConfirmedBooking(
                descCaptor.capture(), numCaptor.capture(),
                dateCaptor.capture(), stayCaptor.capture(),
                nameCaptor.capture(), vendorCaptor.capture(),
                cardNumCaptor.capture(), costCaptor.capture(),
                confNumCaptor.capture());

        verify(bookingUI).setState(uiStateCaptor.capture());

        assertEquals("Description", descCaptor.getValue());
        assertEquals(101, (int) numCaptor.getValue());
        assertEquals(arrivalDate, dateCaptor.getValue());
        assertEquals(1, (int) stayCaptor.getValue());
        assertEquals("A_Name", nameCaptor.getValue());
        assertEquals("Visa", vendorCaptor.getValue());
        assertEquals(1, (int) cardNumCaptor.getValue());
        assertEquals(cost, costCaptor.getValue(), 0.001);
        assertEquals(confNum, (long) confNumCaptor.getValue());
        assertSame(BookingUI.State.COMPLETED, uiStateCaptor.getValue());
        assertSame(BookingCTL.State.COMPLETED, control.state);
    }


    @Test
    void testCreditDetailsEntered_Declined(){
        //arrange
        control.state = BookingCTL.State.CREDIT;
        control.room = room;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;
        control.guest = guest;
        control.occupantNumber = occupantNumber;
        control.cost = cost;
        cardNum = 7;

        String expectedMessage = String.format("Credit Card not approved");
        ArgumentCaptor<String> errorCaptor = ArgumentCaptor.forClass(String.class);

        assertTrue(control.state == BookingCTL.State.CREDIT);

        when(creditCardHelper.makeCreditCard(any(), anyInt(), anyInt())).thenReturn(creditCard);
        when(authorizer.authorize(any(), anyDouble())).thenReturn(false);


        //act
        control.creditDetailsEntered(cardType,cardNum,ccv);


        // assert

        verify(creditCardHelper).makeCreditCard(any(), anyInt(), anyInt());
        verify(authorizer).authorize(creditCard, cost);
        assertSame(control.state, BookingCTL.State.CREDIT);
        verify(bookingUI).displayMessage(errorCaptor.capture());
        assertEquals(expectedMessage, errorCaptor.getValue());
    }



    @Test
    void testCreditDetailsEntered_Exception_WrongState(){
        //arrange
        control.state = BookingCTL.State.COMPLETED;
        assertNotSame(control.state, BookingCTL.State.CREDIT);

        //act
        Executable e = () -> control.creditDetailsEntered(cardType, cardNum, 1);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertEquals("BookingCTL: creditDetailsEntered : bad state : " + control.state, t.getMessage());
    }


    @Test
    void testBookingTimesEntered_Exception_WrongState(){
        //arrange
        control.state = BookingCTL.State.COMPLETED;
        assertNotSame(control.state, BookingCTL.State.TIMES);

        //act
        Executable e = () -> control.bookingTimesEntered(arrivalDate, stayLength);
        Throwable t = assertThrows(RuntimeException.class, e);

        // assert
        assertEquals("BookingCTL: bookingTimesEntered : bad state : " + control.state, t.getMessage());
    }


    @Test
    void testBookingTimesEntered_RoomAvailable(){
        //arrange
        control.state = BookingCTL.State.TIMES;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;
        control.guest = guest;
        control.occupantNumber = occupantNumber;
        control.selectedRoomType = selectedRoomType;

        assertSame(control.state, BookingCTL.State.TIMES);
        when(hotel.findAvailableRoom(selectedRoomType, arrivalDate, stayLength))
                .thenReturn(room);

        ArgumentCaptor<String> roomDescriptionCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Date> arrivalDateCaptor = ArgumentCaptor.forClass(Date.class);
        ArgumentCaptor<Integer> stayLengthCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Double> costCaptor = ArgumentCaptor.forClass(Double.class);

        //act
        control.bookingTimesEntered(arrivalDate, stayLength);

        // assert
        verify(hotel).findAvailableRoom(selectedRoomType, arrivalDate, stayLength);
        verify(bookingUI).displayBookingDetails(roomDescriptionCaptor.capture(),
                arrivalDateCaptor.capture(), stayLengthCaptor.capture(),
                costCaptor.capture());
        verify(bookingUI).setState(uiStateCaptor.capture());

        assertEquals("Single room", roomDescriptionCaptor.getValue());
        assertEquals(arrivalDate, arrivalDateCaptor.getValue());
        assertEquals(1, (int) stayLengthCaptor.getValue());
        assertSame(BookingUI.State.CREDIT, uiStateCaptor.getValue());
        assertSame(BookingCTL.State.CREDIT, control.state);
    }


    @Test
    void testBookingTimesEntered_RoomNotAvailable(){
        //arrange
        control.state = BookingCTL.State.TIMES;
        control.arrivalDate = arrivalDate;
        control.stayLength = stayLength;
        control.guest = guest;
        control.occupantNumber = occupantNumber;
        control.selectedRoomType = selectedRoomType;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(arrivalDate);
        calendar.add(Calendar.DATE, stayLength);
        Date departureDate = calendar.getTime();
        String formatDepartureDate = format.format(departureDate);
        String formatArrivalDate = format.format(arrivalDate);

        assertSame(control.state, BookingCTL.State.TIMES);
        when(hotel.findAvailableRoom(selectedRoomType, arrivalDate, stayLength))
                .thenReturn(null);

        //act
        control.bookingTimesEntered(arrivalDate, stayLength);

        // assert
        verify(bookingUI).displayMessage("\n" + roomDescription + " is not available between " +
                formatArrivalDate + " and " + formatDepartureDate + "\n");
        assertSame(BookingCTL.State.TIMES, control.state);
    }
}


