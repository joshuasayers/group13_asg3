package hotel.checkin;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.Guest;
import hotel.entities.Hotel;
import hotel.entities.Room;
import hotel.entities.RoomType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CheckinUC_ScenarioTesting {

    @Mock CheckinUI ui;

    SimpleDateFormat format;
    Date date;
    Hotel hotel;
    Room room;
    Guest guest;
    int stayLen = 1;
    int numOccupants = 1;
    long confirmationNumber;
    CreditCard card;
    CheckinCTL checkinController;

    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        format = new SimpleDateFormat("dd-MM-yyyy");
        date = format.parse("13-10-2018");

        hotel = new Hotel();
        room = new Room(101, RoomType.SINGLE);
        guest = new Guest("Name", "address", 1);
        card = new CreditCard(CreditCardType.VISA, 1234, 111);

        checkinController = new CheckinCTL(hotel);
        checkinController.checkInUI = ui;
    }

    @AfterEach
    void tearDown() throws Exception {

    }

    @Test
    void successfulCheckin() {
        // arrange
        confirmationNumber = hotel.book(room, guest, date, stayLen, numOccupants, card);

        // act
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        checkinController.confirmationNumberEntered(confirmationNumber);
        assertTrue(checkinController.state == CheckinCTL.State.CONFIRMING);
        checkinController.checkInConfirmed(true);
        assertTrue(checkinController.state == CheckinCTL.State.COMPLETED);

        // assert
        verify(ui).displayCheckinMessage(room.getDescription(), room.getId(), date, stayLen, guest.getName(),
                card.getVendor(), card.getNumber(), confirmationNumber);
        verify(ui).displayMessage(String.format("Check In of booking %s is now complete.", confirmationNumber));
        verify(ui, times(2)).setState(any(CheckinUI.State.class));

        assertNotNull(hotel.findActiveBookingByRoomId(room.getId()));
        assertTrue(hotel.findActiveBookingByRoomId(room.getId()).isCheckedIn());
        assertTrue(hotel.findActiveBookingByRoomId(room.getId()).getRoom().isOccupied());
    }

    @Test
    void noBookingFound() {
        // arrange
        long badConfirmationNumber = 123456;

        // act
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        checkinController.confirmationNumberEntered(badConfirmationNumber);

        // assert
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        verify(ui).displayMessage(String.format("No booking for confirmation number %d found", badConfirmationNumber));
        verify(ui, times(0)).setState(any(CheckinUI.State.class));
    }

    @Test
    void alreadyCheckedIn() {
        // arrange
        confirmationNumber = hotel.book(room, guest, date, stayLen, numOccupants, card);
        hotel.checkin(confirmationNumber);
        assertTrue(hotel.findActiveBookingByRoomId(room.getId()).isCheckedIn());

        // act
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        checkinController.confirmationNumberEntered(confirmationNumber);

        // assert
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        verify(ui).displayMessage(String.format("Booking %d has already been checked in", confirmationNumber));
        verify(ui, times(0)).setState(any(CheckinUI.State.class));
    }

    @Test
    void alreadyCheckedOut() {
        // arrange
        confirmationNumber = hotel.book(room, guest, date, stayLen, numOccupants, card);
        // easy way to get a Booking into the CHECKED_OUT state is to simply checkin then checkout.
        hotel.checkin(confirmationNumber);
        hotel.checkout(room.getId());
        // confirm the Booking is checked out.
        assertTrue(hotel.findBookingByConfirmationNumber(confirmationNumber).isCheckedOut());

        // act
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        checkinController.confirmationNumberEntered(confirmationNumber);


        // assert
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        verify(ui).displayMessage(String.format(String.format("Booking %d has already been checked out", confirmationNumber)));
        verify(ui, times(0)).setState(any(CheckinUI.State.class));
    }

    @Test
    void roomNotReady() throws Exception {
        // arrange
        Date anotherDate = format.parse("14-10-2018");
        confirmationNumber = hotel.book(room, guest, date, stayLen, numOccupants, card);
        long anotherConfirmationNumber = hotel.book(room, guest, anotherDate, stayLen, numOccupants, card);
        assertTrue(room.isReady());

        // act
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        checkinController.confirmationNumberEntered(confirmationNumber);
        assertTrue(checkinController.state == CheckinCTL.State.CONFIRMING);
        checkinController.checkInConfirmed(true);
        assertTrue(checkinController.state == CheckinCTL.State.COMPLETED);
        assertTrue(room.isOccupied());

        checkinController.state = CheckinCTL.State.CHECKING;
        checkinController.confirmationNumberEntered(anotherConfirmationNumber);

        // assert
        assertTrue(checkinController.state == CheckinCTL.State.CHECKING);
        verify(ui).displayMessage("Room is not ready, sorry");
        verify(ui, times(2)).setState(any(CheckinUI.State.class));
    }

}
