package hotel.entities;

import hotel.credit.CreditCard;
import hotel.helpers.ServiceChargeHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


@ExtendWith(MockitoExtension.class)


class TestBookingRoomIntegration {

    Booking booking;
    Room room;
    Guest guest;
    @Mock CreditCard mockCard;
    Date date = new GregorianCalendar(2018, 10, 5).getTime();
    int stayLength = 1;
    int numberOfOccupants = 1;


    @BeforeEach
       void setUp() throws RuntimeException{
        guest = new Guest("Name", "Address", 123);
        room = new Room(101, RoomType.SINGLE);
        booking = new Booking(guest, room, date, stayLength, numberOfOccupants, mockCard);

   }


    @AfterEach
    void tearDown() throws RuntimeException {
    }


    @Test
    void testCheckInIf_BookingPending_RoomReady(){
    //arrange
        assertTrue(booking.isPending());

    //act
        booking.checkIn();

    //assert
        assertTrue(booking.isCheckedIn());
        assertFalse(room.isReady());
    }

    @Test
    void testCheckInIf_BookingPending_RoomNotReady(){
        //arrange
        room.checkin();
        assertTrue(booking.isPending());
        assertFalse(room.isReady());

        //act
        Executable e = () -> room.checkin();
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals("Room: checkin : bad state : " + room.state, t.getMessage());
    }


    @Test
    void testCheckOutIf_BookingCheckedIn_RoomOccupied(){
        //arrange
        booking.checkIn();
        assertTrue(booking.isCheckedIn());

        //act
        booking.checkOut();

        //assert
        assertTrue(booking.isCheckedOut());
        assertTrue(room.isReady());
    }

    @Test
    void testCheckOutIf_BookingCheckedIn_RoomReadyException(){
        //arrange
        booking.checkIn();
        assertTrue(booking.isCheckedIn());
        assertFalse(room.isReady());

        //act
        room.state = Room.State.READY;
        Executable e = () -> room.checkout(booking);
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals("Room: checkout : bad state : " + room.state, t.getMessage());

    }


    @Test
    void testAddServiceCharge_BookingCheckedIn(){
        //arrange
        booking.checkIn();
        assertTrue(booking.isCheckedIn());
        assertFalse(room.isReady());

        //act
        room.state = Room.State.READY;
        Executable e = () -> room.checkout(booking);
        Throwable t = assertThrows(RuntimeException.class, e);

        //assert
        assertEquals("Room: checkout : bad state : " + room.state, t.getMessage());

    }
}