package hotel.helpers;

import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;



public class CreditCardHelper {
    public static CreditCardHelper self;

    public CreditCardHelper() {
    }

    public static CreditCardHelper getInstance() {
        if(self == null) {
            self = new CreditCardHelper();
        }
       return self;
    }

    public CreditCard makeCreditCard(CreditCardType type, int num, int ccv) {
        return new CreditCard(type, num, ccv);
    }
}
