package hotel.helpers;

import hotel.entities.Guest;

public class GuestHelper {
    private static GuestHelper self;

    public GuestHelper() {}

    public static GuestHelper getInstance() {
        if(self == null) {
            self = new GuestHelper();
        }
        return self;
    }

    public Guest makeGuest(String n, String a, int p) {
        return new Guest(n, a, p);
    }
}
