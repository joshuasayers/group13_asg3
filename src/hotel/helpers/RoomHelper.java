package hotel.helpers;

import hotel.credit.CreditCard;
import hotel.entities.*;

import java.util.Date;

public class RoomHelper {
    private static RoomHelper self;

    public RoomHelper() {}

    public static RoomHelper getInstance() {
        if(self == null) {
            self = new RoomHelper();
        }
        return self;
    }

    public Room makeRoom(int n, RoomType t) {
        return new Room(n, t);
    }
}
