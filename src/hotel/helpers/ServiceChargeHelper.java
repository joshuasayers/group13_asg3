package hotel.helpers;

import hotel.entities.ServiceCharge;
import hotel.entities.ServiceType;

public class ServiceChargeHelper {
    private static ServiceChargeHelper self;

    public ServiceChargeHelper() {}

    public static ServiceChargeHelper getInstance() {
        if(self == null) {
            self = new ServiceChargeHelper();
        }
        return self;
    }

    public ServiceCharge makeServiceCharge(ServiceType st, double c) {
        return new ServiceCharge(st, c);
    }
}
